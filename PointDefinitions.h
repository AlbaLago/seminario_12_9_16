#ifndef POINTDEFINITIONS_H
#define POINTDEFINITIONS_H
#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <string>
struct PointMMS
{
  PCL_ADD_POINT4D;                  // Añadimos los valores XYZ+padding SSE
  PCL_ADD_INTENSITY;                // Añadimos valor de intensidad
  PCL_ADD_NORMAL4D                  // Añadimos componentes de las normales
  double timeStamp;                 // Añadimos valor para almacenar timestamp
  int angle;                        // Añadimos valor para almacenar el scan angle rank
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment

POINT_CLOUD_REGISTER_POINT_STRUCT (PointMMS,           // Realizamos el registro de las estructuras
                                   (float, x, x)
                                   (float, y, y)
                                   (float, z, z)
                                   (double, timeStamp, timeStamp)
                                   (int, angle, angle)
)

struct PointTraj
{
  PCL_ADD_POINT4D;                  // Añadimos los valores XYZ+padding SSE
  double timeStamp;                 // Añadimos valor para almacenar timestamp
  double odometerLecture;           // Añadimos valor para almacenar lectura del odometro
  float roll,pitch,yaw;             // Añadimos valor para almacenar datos del inercial
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW   // make sure our new allocators are aligned
} EIGEN_ALIGN16;                    // enforce SSE padding for correct memory alignment

POINT_CLOUD_REGISTER_POINT_STRUCT (PointTraj,           // Realizamos el registro de las estructuras
                                   (float, x, x)
                                   (float, y, y)
                                   (float, z, z)
                                   (double, timeStamp, timeStamp)
                                   (double, odometerLecture, odometerLecture)
                                   (float, roll, roll)
                                   (float, pitch, pitch)
                                   (float, yaw, yaw)
)

class PointDefinitions
{
public:
    PointDefinitions();
    ~PointDefinitions();
};

#endif // POINTDEFINITIONS_H
